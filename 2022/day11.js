/**
 * https://adventofcode.com/2022/day/11
 * 
 * Live coding available on YouTube
 * - Part 1: https://youtu.be/hUDmu4tHSNw
 * - Part 2: https://youtu.be/eTwnpnmvp9s
 */

 const input = `Monkey 0:
 Starting items: 59, 65, 86, 56, 74, 57, 56
 Operation: new = old * 17
 Test: divisible by 3
   If true: throw to monkey 3
   If false: throw to monkey 6
 
 Monkey 1:
 Starting items: 63, 83, 50, 63, 56
 Operation: new = old + 2
 Test: divisible by 13
   If true: throw to monkey 3
   If false: throw to monkey 0
 
 Monkey 2:
 Starting items: 93, 79, 74, 55
 Operation: new = old + 1
 Test: divisible by 2
   If true: throw to monkey 0
   If false: throw to monkey 1
 
 Monkey 3:
 Starting items: 86, 61, 67, 88, 94, 69, 56, 91
 Operation: new = old + 7
 Test: divisible by 11
   If true: throw to monkey 6
   If false: throw to monkey 7
 
 Monkey 4:
 Starting items: 76, 50, 51
 Operation: new = old * old
 Test: divisible by 19
   If true: throw to monkey 2
   If false: throw to monkey 5
 
 Monkey 5:
 Starting items: 77, 76
 Operation: new = old + 8
 Test: divisible by 17
   If true: throw to monkey 2
   If false: throw to monkey 1
 
 Monkey 6:
 Starting items: 74
 Operation: new = old * 2
 Test: divisible by 5
   If true: throw to monkey 4
   If false: throw to monkey 7
 
 Monkey 7:
 Starting items: 86, 85, 52, 86, 91, 95
 Operation: new = old + 6
 Test: divisible by 7
   If true: throw to monkey 4
   If false: throw to monkey 5`;
 
 // Function to generate a monkey list
 
 function generateMonkeys(input) {
   let monkeys = [];
   let lines = input.split('\n');
   
   for (let monkeyIndex = 0; monkeyIndex < lines.length; monkeyIndex += 7) {
     monkeys.push({
       number: monkeys.length,
       itemWorryLevelList: lines[monkeyIndex + 1].replace('Starting items: ', '').split(', ').map(itemWorryLevel => parseInt(itemWorryLevel)),
       operation: lines[monkeyIndex + 2].replace('Operation: new = ', ''),
       test: {
         isDivisibleBy: parseInt(lines[monkeyIndex + 3].replace('Test: divisible by ', '')),
         isTrueMonkey: parseInt(lines[monkeyIndex + 4].replace('If true: throw to monkey ', '')),
         isFalseMonkey: parseInt(lines[monkeyIndex + 5].replace('If false: throw to monkey ', '')),
       },
       itemsInspected: 0,
     });
   }
 
   return monkeys;
 }
 
 // Function to play the rounds
 
 function playRounds(monkeys, rounds, applyRelief = true) {
   for (let round = 0; round < rounds; ++round) {
     monkeys
     .forEach((monkey) => {
       monkey = inspectItems(monkey, monkeys, applyRelief);
       monkeys = testMonkey(monkey, monkeys);
     });
   }
 
   return monkeys;
 }
 
 // Function to inspect an item
 
 function inspectItems(monkey, monkeys, applyRelief = true) {
   let allDivisorsModulo = monkeys
     .map(monkey => monkey.test.isDivisibleBy)
     .reduce((modulo, divisor) => modulo * divisor, 1);
 
   monkey.itemsInspected += monkey.itemWorryLevelList.length;
   monkey.itemWorryLevelList = monkey.itemWorryLevelList
     .map(itemWorryLevel => {
       let newItemWorryLevel = evaluateMonkeyOperation(monkey.operation, itemWorryLevel);
       return applyRelief ? reliefItem(newItemWorryLevel) : newItemWorryLevel % allDivisorsModulo
     });
 
   return monkey;
 }
 
 // Function to relief the worry level of an item
 
 function reliefItem(itemWorryLevel) {
   return Math.floor(itemWorryLevel / 3);
 }
 
 // Function to evaluate monkey's operation
 
 function evaluateMonkeyOperation(operation, itemWorryLevel) {
   return eval(operation.replace(/old/g, itemWorryLevel))
 }
 
 // Function to evaluate monkey's test
 
 function testMonkey(currentMonkey, monkeys) {
   currentMonkey.itemWorryLevelList
     .forEach(itemWorryLevel => {
       if (itemWorryLevel % currentMonkey.test.isDivisibleBy === 0) {
         monkeys[currentMonkey.test.isTrueMonkey].itemWorryLevelList.push(itemWorryLevel);
       } else {
         monkeys[currentMonkey.test.isFalseMonkey].itemWorryLevelList.push(itemWorryLevel);
       }
     })
 
   monkeys[currentMonkey.number].itemWorryLevelList = [];
 
   return monkeys;
 }
 
 // Play first part
 
 let monkeys1 = generateMonkeys(input);
 monkeys1 = playRounds(monkeys1, 20);
 monkeys1.sort((m1, m2) => m2.itemsInspected - m1.itemsInspected);
 
 // Get first answer
 
 let answer1 = monkeys1[0].itemsInspected * monkeys1[1].itemsInspected;
 
 // Play second part
 
 let monkeys2 = generateMonkeys(input);
 monkeys2 = playRounds(monkeys2, 10000, false);
 monkeys2.sort((m1, m2) => m2.itemsInspected - m1.itemsInspected);
 
 // Get second answer
 
 let answer2 = monkeys2[0].itemsInspected * monkeys2[1].itemsInspected;
 
 // Output answers
 
 console.log(answer1, answer2);
 