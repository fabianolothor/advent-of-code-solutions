/**
 * https://adventofcode.com/2022/day/10
 *
 * Solution starts at #L153
 * - https://gitlab.com/fabianolothor/advent-of-code-solutions/-/blob/main/2022/day10.js#L153
 *
 * Live coding available on YouTube
 * - Part 1: https://youtu.be/GvIeeYJiDBg
 * - Part 2: https://youtu.be/vWTvXnpDjr8
 */

const input = `addx 1
noop
addx 4
noop
noop
noop
addx 6
addx -1
addx 5
noop
noop
noop
addx 5
addx -14
noop
addx 19
noop
addx 1
addx 4
addx 1
noop
noop
addx 2
addx 5
addx -27
addx 20
addx -30
addx 2
addx 5
addx 2
addx 4
addx -3
addx 2
addx 5
addx 2
addx -9
addx 1
addx 11
noop
addx -20
addx 7
addx 23
addx 2
addx 3
addx -2
addx -34
addx -2
noop
addx 3
noop
addx 3
addx 2
noop
addx 3
addx 2
addx 5
addx 2
addx -9
addx -7
addx 21
noop
addx 8
noop
addx -1
addx 3
addx -2
addx 5
addx -37
noop
addx 35
addx -31
addx 1
addx 4
addx -1
addx 2
noop
addx 3
addx 1
addx 5
addx -2
addx 7
addx -2
addx -2
addx 10
noop
addx 4
noop
noop
addx -19
addx 20
addx -38
noop
noop
addx 7
addx 2
addx 3
noop
addx 4
addx -3
addx 2
addx 2
noop
addx 3
noop
noop
noop
addx 5
noop
addx 7
addx -2
addx 7
noop
noop
addx -5
addx 6
addx -36
noop
addx 1
addx 2
addx 5
addx 2
addx 3
addx -2
addx 2
addx 5
addx 2
addx 1
noop
addx 4
addx -16
addx 21
noop
noop
addx 1
addx -8
addx 12
noop
noop
noop
noop`;

// Global variables

let x = 1;
let currentCycle = 0;
let signalStrengths = { sum: 0 };
let crt = {};

// Function to register the signal strengths

function registerStrength() {
  if (currentCycle !== 20 && (currentCycle - 20) % 40 !== 0) {
    return;
  }
  
  let signalStrength = currentCycle * x;

  signalStrengths['sum'] += signalStrength
  signalStrengths[currentCycle] = signalStrength;
}

// Function to draw in the CRT system

function draw() {
  let line = Math.floor(currentCycle / 40) + 1;

  if (!crt[line]) {
    crt[line] = '';
  }

  xPosition = x + ((line - 1) * 40);

  crt[line] += currentCycle >= xPosition - 1 && currentCycle <= xPosition + 1 ? '#' : '.';
}

// Process the input

input
  .split('\n')
  .forEach(instruction => {
    draw();
    ++currentCycle;
    registerStrength();

    let [method, parameter] = instruction.split(' ');


    if (method === 'addx') {
      draw();
      ++currentCycle;
      registerStrength();
      
      x += parseInt(parameter);
    }
  });

// Get first answer

let answer1 = signalStrengths['sum'];

// Get second answer

let answer2 = crt;

// Output the answers

console.log(answer1, answer2);
